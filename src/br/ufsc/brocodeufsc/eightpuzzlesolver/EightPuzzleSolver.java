package br.ufsc.brocodeufsc.eightpuzzlesolver;

import br.ufsc.brocodeufsc.eightpuzzlesolver.engine.Engine;
import br.ufsc.brocodeufsc.eightpuzzlesolver.engine.Solution;
import br.ufsc.brocodeufsc.eightpuzzlesolver.exception.NoSolutionException;
import br.ufsc.brocodeufsc.eightpuzzlesolver.model.Board;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.view.UI;

public class EightPuzzleSolver {

	public static void main(final String[] args) {
		final Board board = UI.askBoard();
		UI.printBoard(board);
		UI.println("Resolvendo...");
		final Engine engine = new Engine();
		try {
			final Solution solution = engine.solve(board);
			if(solution.getSteps() != null)
				UI.printSolution(solution.getSteps());
			else
				UI.println("O tabuleiro j� est� resolvido!");
			UI.println("Tamanho m�ximo da fronteira: " + solution.getMaxFrontierSize());
		} catch(final NoSolutionException e) {
			UI.println("N�o consegui resolver :(");
		} catch(final Exception e) {
			UI.println("N�o consegui resolver :(");
			e.printStackTrace();
		}
	}

}
