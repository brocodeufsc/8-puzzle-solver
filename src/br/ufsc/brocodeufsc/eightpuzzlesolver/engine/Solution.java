package br.ufsc.brocodeufsc.eightpuzzlesolver.engine;

import java.util.List;

import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Direction;

public class Solution {

	private List<Direction> steps;
	private Integer maxFrontierSize;

	public Solution(final List<Direction> steps, final Integer maxFrontierSize) {
		this.steps = steps;
		this.maxFrontierSize = maxFrontierSize;
	}

	public List<Direction> getSteps() {
		return steps;
	}

	public void setSteps(final List<Direction> steps) {
		this.steps = steps;
	}

	public Integer getMaxFrontierSize() {
		return maxFrontierSize;
	}

	public void setMaxFrontierSize(final Integer maxFrontierSize) {
		this.maxFrontierSize = maxFrontierSize;
	}

}
