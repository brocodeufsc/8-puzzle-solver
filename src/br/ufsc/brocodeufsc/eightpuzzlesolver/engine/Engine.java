package br.ufsc.brocodeufsc.eightpuzzlesolver.engine;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import br.ufsc.brocodeufsc.eightpuzzlesolver.exception.NoSolutionException;
import br.ufsc.brocodeufsc.eightpuzzlesolver.model.Board;
import br.ufsc.brocodeufsc.eightpuzzlesolver.model.BoardState;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Direction;

public class Engine {

	private final Queue<BoardState> frontier;
	private final List<BoardState> visited;

	private Integer maxFrontierSize = 0;

	public Engine() {
		frontier = new PriorityQueue<>();
		visited = new LinkedList<>();
	}

	private void addToFrontier(final BoardState boardState) {
		frontier.add(boardState);
		if(frontier.size() > maxFrontierSize) {
			maxFrontierSize = frontier.size();
		}
	}

	public Solution solve(final Board board) {
		final BoardState root = new BoardState(board);
		addToFrontier(root);
		final List<Direction> steps = solve(root);
		return new Solution(steps, maxFrontierSize);
	}

	private List<Direction> solve(final BoardState current) {
		if(current.isGoal()) {
			return current.getPath();
		}

		visited.add(current);
		frontier.remove(current);

		final List<BoardState> children = current.createChildren();

		for(final BoardState child : children) {
			if(!visited.contains(child) && !frontier.contains(child)) {
				addToFrontier(child);
			}
		}

		final BoardState next = frontier.poll();
		if(next != null) {
			final List<Direction> solution = solve(next);
			if(solution != null) {
				return solution;
			}
		}

		if(!current.isRootState()) {
			return null;
		}

		throw new NoSolutionException();
	}

}
