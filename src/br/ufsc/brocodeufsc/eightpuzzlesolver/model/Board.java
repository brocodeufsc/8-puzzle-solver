package br.ufsc.brocodeufsc.eightpuzzlesolver.model;

import java.util.Optional;

import br.ufsc.brocodeufsc.eightpuzzlesolver.exception.TileNotFoundException;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Position;

public class Board {

	public static final Board GOAL = new Board(new Integer[][] {
		{ 1, 2, 3 },
		{ 4, 5, 6 },
		{ 7, 8, 0 }
	});

	private Optional<Integer>[][] board;

	public Board() {
		this(new Integer[3][3]);
	}

	@SuppressWarnings("unchecked")
	public Board(final Integer[][] board) {
		this.board = new Optional[3][3];
		for(int l = 0; l < board.length; l++) {
			for(int c = 0; c < board[l].length; c++) {
				final Integer num = board[l][c];
				if(num != null) {
					this.board[l][c] = num == 0 ? Optional.empty() : Optional.of(num);
				} else {
					this.board[l][c] = null;
				}
			}
		}
	}

	public Board move(final Position destination) {
		final Integer[][] newRawBoard = new Integer[3][3];
		for(int l = 0; l < board.length; l++) {
			for(int c = 0; c < board[l].length; c++) {
				newRawBoard[l][c] = board[l][c].orElse(null);
			}
		}
		final Board newBoard = new Board(newRawBoard);
		final Optional<Integer> number = get(destination);
		newBoard.set(findBlankTile(), number);
		newBoard.set(destination, Optional.empty());
		return newBoard;
	}

	public Position find(final Optional<Integer> tile) {
		if(tile.isPresent())
			return findNumber(tile.get());
		return findBlankTile();
	}

	public Position findBlankTile() {
		for(int l = 0; l < board.length; l++) {
			for(int c = 0; c < board[l].length; c++) {
				if(!board[l][c].isPresent())
					return new Position(l, c);
			}
		}
		throw new TileNotFoundException();
	}

	@Override
	public boolean equals(final Object obj) {
		if(obj instanceof Board) {
			final Board other = (Board) obj;
			for(int l = 0; l < board.length; l++) {
				for(int c = 0; c < board[l].length; c++) {
					if(!board[l][c].equals(other.board[l][c])) {
						return false;
					}
				}
			}
			return true;
		}
		return super.equals(obj);
	}

	public Optional<Integer>[][] getBoard() {
		return board;
	}

	public void setBoard(final Optional<Integer>[][] board) {
		this.board = board;
	}

	public Optional<Integer> get(final Position position) {
		return board[position.getLine()][position.getColumn()];
	}

	private void set(final Position position, final Optional<Integer> value) {
		board[position.getLine()][position.getColumn()] = value;
	}

	private Position findNumber(final Integer number) {
		for(int l = 0; l < board.length; l++) {
			for(int c = 0; c < board[l].length; c++) {
				final Optional<Integer> tile = board[l][c];
				if(tile.isPresent() && number.equals(tile.get()))
					return new Position(l, c);
			}
		}
		throw new TileNotFoundException();
	}

}
