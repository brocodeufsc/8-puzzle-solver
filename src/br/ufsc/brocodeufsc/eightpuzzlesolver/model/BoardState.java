package br.ufsc.brocodeufsc.eightpuzzlesolver.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import br.ufsc.brocodeufsc.eightpuzzlesolver.util.Heuristic;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.State;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Direction;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Position;

public class BoardState implements State<BoardState>, Heuristic, Comparable<BoardState> {

	private Board board;

	private final BoardState ancestor;
	private final Direction chosenDirection;

	private final Integer hValue;
	private final Integer cost;

	public BoardState(final Board board) {
		this(board, null, null);
	}

	public BoardState(final Board board, final BoardState ancestor, final Direction chosenDirection) {
		this.board = board;

		this.ancestor = ancestor;
		this.chosenDirection = chosenDirection;

		hValue = h();
		cost = getLevel();
	}

	private Integer getLevel() {
		if(ancestor == null)
			return 0;
		return ancestor.getLevel() + 1;
	}

	@Override
	public boolean isGoal() {
		return hValue == 0;
	}

	@Override
	public BoardState getAncestor() {
		return ancestor;
	}

	@Override
	public List<BoardState> createChildren() {
		final List<BoardState> children = new ArrayList<BoardState>();
		final Position blankTilePosition = board.findBlankTile();

		for(final Direction direction : Direction.values()) {
			if(direction.canMove(blankTilePosition)) {
				final BoardState child = new BoardState(board.move(direction.move(blankTilePosition)), this, direction);
				children.add(child);
			}
		}
		return children;
	}

	public List<Direction> getPath() {
		if(ancestor == null)
			return null;
		final List<Direction> path;
		if(ancestor.isRootState()) {
			path = new LinkedList<>();
			path.add(chosenDirection);
			return path;
		}
		path = ancestor.getPath();
		path.add(chosenDirection);
		return path;
	}

	@Override
	public Integer h() {
		return getManhattanDistance();
	}

	private Integer getManhattanDistance() {
		Integer totalDistance = 0;
		final Optional<Integer>[][] b = board.getBoard();
		for(int l = 0; l < b.length; l++) {
			for(int c = 0; c < b[l].length; c++) {
				final Optional<Integer> tile = b[l][c];
				if(tile.isPresent()) {
					final Position actual = new Position(l, c);
					final Position expectedPosition = Board.GOAL.find(tile);
					final Integer distance = Math.abs(actual.getLine() - expectedPosition.getLine())
						+ Math.abs(actual.getColumn() - expectedPosition.getColumn());
					totalDistance += distance;
				}
			}
		}
		return totalDistance;
	}

	private Integer getMisplacedTilesCount() {
		Integer misplacedTiles = 0;
		final Optional<Integer>[][] b = board.getBoard();
		for(int l = 0; l < b.length; l++) {
			for(int c = 0; c < b[l].length; c++) {
				final Optional<Integer> tile = b[l][c];
				if(tile.isPresent()) {
					final Position actual = new Position(l, c);
					final Position expectedPosition = Board.GOAL.find(tile);
					if(actual.getLine() != expectedPosition.getLine() || actual.getColumn() != expectedPosition.getColumn()) {
						misplacedTiles++;
					}
				}
			}
		}
		return misplacedTiles;
	}

	@Override
	public int compareTo(final BoardState o) {
		final int comparation = getScore().compareTo(o.getScore());
		if(comparation != 0) {
			return comparation;
		}
		return getMisplacedTilesCount().compareTo(o.getMisplacedTilesCount());
	}

	@Override
	public boolean equals(final Object obj) {
		if(obj instanceof BoardState) {
			final BoardState other = (BoardState) obj;
			return compareTo(other) == 0 && board.equals(other.board);
		}
		return super.equals(obj);
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(final Board board) {
		this.board = board;
	}

	public Direction getChosenDirection() {
		return chosenDirection;
	}

	public Integer getScore() {
		return hValue + cost;
	}

}
