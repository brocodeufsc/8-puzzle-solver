package br.ufsc.brocodeufsc.eightpuzzlesolver.util;

import java.util.List;

@SuppressWarnings("rawtypes")
public interface State<T extends State> {

	boolean isGoal();

	default boolean isRootState() {
		return getAncestor() == null;
	}

	T getAncestor();

	List<T> createChildren();

}
