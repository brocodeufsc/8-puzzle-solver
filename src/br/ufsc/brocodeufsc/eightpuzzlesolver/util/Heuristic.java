package br.ufsc.brocodeufsc.eightpuzzlesolver.util;

public interface Heuristic {

	Integer h();

}
