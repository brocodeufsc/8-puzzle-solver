package br.ufsc.brocodeufsc.eightpuzzlesolver.util.view;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import br.ufsc.brocodeufsc.eightpuzzlesolver.model.Board;
import br.ufsc.brocodeufsc.eightpuzzlesolver.util.board.Direction;

public class UI {

	public static void printBoard(final Board board) {
		println(" -------");
		for(final Optional<Integer>[] line : board.getBoard()) {
			print("| ");
			for(final Optional<Integer> num : line) {
				if(num != null) {
					print(num.isPresent() ? num.get() : "X");
				} else {
					print(" ");
				}
				print(" ");
			}
			println("|");
		}
		println(" -------");
	}

	public static void printSolution(final List<Direction> solution) {
		println("Solu��o:");
		for(int i = 0; i < solution.size(); i++) {
			final Direction d = solution.get(i);
			println(i + 1 + "\t> " + d);
		}
	}

	public static void println(final Object msg) {
		System.out.println(msg);
	}

	public static void print(final Object msg) {
		System.out.print(msg);
	}

	public static Board askBoard() {
		@SuppressWarnings("resource")
		final Scanner input = new Scanner(System.in);
		final Board board = new Board();
		final Optional<Integer>[][] b = board.getBoard();
		for(int l = 0; l < b.length; l++) {
			for(int c = 0; c < b[l].length; c++) {
				println("Entrada:");
				printBoard(board);
				println("Digite \"0\" para espa�o vazio");
				print("> ");
				final Integer next = input.nextInt();
				b[l][c] = next == 0 ? Optional.empty() : Optional.of(next);
			}
		}
		return board;
	}

}
