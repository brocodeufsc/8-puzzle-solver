package br.ufsc.brocodeufsc.eightpuzzlesolver.util.board;

public class Position {

	private Integer line;

	private Integer column;

	public Position() {
		this(0, 0);
	}

	public Position(final Integer line, final Integer column) {
		this.line = line;
		this.column = column;
	}

	public Position getSucessor() {
		if(column == 2) {
			if(line == 2) {
				return new Position();
			}
			return new Position(line + 1, 0);
		}
		return new Position(line, column + 1);
	}

	public Integer getLine() {
		return line;
	}

	public void setLine(final Integer line) {
		this.line = line;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(final Integer column) {
		this.column = column;
	}

}
