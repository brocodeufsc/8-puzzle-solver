package br.ufsc.brocodeufsc.eightpuzzlesolver.util.board;

public enum Direction {

	UP {
		@Override
		public boolean canMove(final Position position) {
			return position.getLine() > 0;
		}

		@Override
		public Position move(final Position position) {
			return new Position(position.getLine() - 1, position.getColumn());
		}
	},
	DOWN {
		@Override
		public boolean canMove(final Position position) {
			return position.getLine() < 2;
		}

		@Override
		public Position move(final Position position) {
			return new Position(position.getLine() + 1, position.getColumn());
		}
	},
	LEFT {
		@Override
		public boolean canMove(final Position position) {
			return position.getColumn() > 0;
		}

		@Override
		public Position move(final Position position) {
			return new Position(position.getLine(), position.getColumn() - 1);
		}
	},
	RIGHT {
		@Override
		public boolean canMove(final Position position) {
			return position.getColumn() < 2;
		}

		@Override
		public Position move(final Position position) {
			return new Position(position.getLine(), position.getColumn() + 1);
		}
	};

	public abstract boolean canMove(Position position);

	public abstract Position move(Position position);

}
